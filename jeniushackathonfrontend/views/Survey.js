import React from 'react';
import { StyleSheet, Text, View, Dimensions, Picker, ActivityIndicator, ScrollView } from 'react-native';
import { Image, Button, Input, CheckBox } from 'react-native-elements';
import { Dropdown } from 'react-native-material-dropdown';
import Reactotron from 'reactotron-react-native'
import axios from 'axios';

import logoJenius from '../assets/logo-jenius.png';


export default class Survey extends React.Component {
  static navigationOptions = {
    title: 'Survey Pengguna',
    headerStyle: {
      backgroundColor: '#20A4DC',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: 'bold',
    },
  };

  state = {
    language: null,
    selectedRisk: 'low',
    listDaerah: [],
    pekerjaan: '',
    kota: '',
    beban: '',
    selectedRisk: '',
  }

  handleCheckbox = (val) => {
    this.setState({ selectedRisk: val })
  }
  
  componentWillMount() {
    const passedCashTag = this.props.navigation.getParam('cashTag', 'bobys')
    Reactotron.log(passedCashTag)
    this.getDaerah();
  }

  insertSurvey = () => {
    const { pekerjaan, kota, beban, selectedRisk } = this.state;
    const passedCashTag = this.props.navigation.getParam('cashTag', 'bobys')

    this.setState({ showSplashScreen: true }, () => {
      setTimeout(() => {
        axios.post(`http://10.10.22.222:8080/data/insert_survey`, { 
          jenisPekerjaan: pekerjaan, 
          jumlahTanggungan: beban,
          kotaTinggal: kota, 
          toleransiRisiko: selectedRisk
        })
        .then(res => {
          Reactotron.log(res)
          this.props.navigation.navigate('Dashboard', { cashTag: passedCashTag })
        })
        .catch(err => {
          this.setState({ showSplashScreen: false })
          Reactotron.log(err)
        })
      }, 2000)
    })

  }

  getDaerah = () => {
      axios.get(`http://10.10.22.222:8080/data/get_umr`)
      .then(res => {
        Reactotron.log(res)
        this.setState({
          listDaerah: res.data,
        })
      })
      .catch(err => {
        Reactotron.log(err)
      })
    }

  render() {

    // const { listDaerah, isLoading } = this.state;
    const { listDaerah, pekerjaan, kota, beban, selectedRisk } = this.state;
   

    if (listDaerah.length < 1) return <ActivityIndicator />

    const renamed = listDaerah.map(({ locationId, location: value, umr }) => ({
      locationId,
      value,
      umr
    }));

    

    delete renamed.locationId
    delete renamed.umr

    let dropdown = [{
        value: 'Pegawai BUMN',
      }, {
    }, {
          value: 'Pegawai Negeri Sipil',
        }, {
    }, {
          value: 'Wiraswasta',
        }, {
    }, {
          value: 'Pegawai Swasta',
        }, {
    }, {
          value: 'Freelance',
        }, {
    }, {
          value: 'Seniman',
        }, {
    }, {
          value: 'Pendidikan',
        }, {
    }, {
          value: 'Polisi / TNI',
        }, {
    }, {
          value: 'Pelajar',
        }, {
    }, {
          value: 'Ibu rumah tangga',
        }, {
    }, {
          value: 'Tidak bekerja',
        }, {
    }, {
          value: 'Pensiun',
        }, {
    }, {
          value: 'Lainnya',
    
    }];

    return (
      <ScrollView>
        <View style={main.container}>
          <View style={main.logoContainer}>
            <Image style={{ height: 48 }} source={logoJenius} />
          </View>
          {/* <View>
            <Text style={main.subheader}>Ekspektasi</Text>
            <Text style={main.text}>Isikan ekspektasi aset Anda beserta target waktu kepemilikan</Text>
          </View> */}
          <View style={[form.group, { marginTop: 40 }]}>
            <Text style={main.subheader}>Profil</Text>
              <Dropdown 
                label='Jenis Pekerjaan'
                data={dropdown}
                onChangeText={pekerjaan => this.setState({ pekerjaan })}
                value={pekerjaan}
                fontSize={16}
              />
              <Dropdown 
                label='Kota Tinggal'
                data={renamed}
                onChangeText={kota => this.setState({ kota })}
                value={kota}
                fontSize={16}
              />
              <Input
                containerStyle={{paddingHorizontal: 0, marginVertical: 16}}
                placeholder='Jumlah Tanggungan'
                onChangeText={beban => this.setState({ beban })}
                value={beban}
              />
          
          </View>
          <View style={form.group}>
            <Text style={main.subheader}>Toleransi Risiko</Text>
            <Text style={main.text}>Kami akan mengolah data dan membuat rekomendasi investasi untuk anda berdasarkan tingkat risiko</Text>
            <View style={{ marginTop: 16 }}>
              <CheckBox
                left
                title='Risiko Rendah'
                checkedIcon='dot-circle-o'
                uncheckedIcon='circle-o'
                containerStyle={{ marginHorizontal: 0, marginVertical: 8 }}
                onPress={() => this.handleCheckbox('low')}
                checked={this.state.selectedRisk === 'low'}
                />
              <CheckBox
                left
                title='Risiko Sedang'
                checkedIcon='dot-circle-o'
                uncheckedIcon='circle-o'
                containerStyle={{ marginHorizontal: 0, marginVertical: 8 }}
                onPress={() => this.handleCheckbox('medium')}
                checked={this.state.selectedRisk === 'medium'}
                />
              <CheckBox
                left
                title='Risiko Tinggi'
                checkedIcon='dot-circle-o'
                uncheckedIcon='circle-o'
                containerStyle={{ marginHorizontal: 0, marginVertical: 8 }}
                onPress={() => this.handleCheckbox('high')}
                checked={this.state.selectedRisk === 'high'}
                
                />
            </View>
          </View>
          <View>
          <Button
            // onPress={() => this.props.navigation.navigate('Recommendation')}
            onPress={this.insertSurvey}
            title="Submit"
            buttonStyle={{
              width: Dimensions.get('window').width - 48,
              height: 48,
              backgroundColor: "#20A4DC",
              position: 'relative',
            }}
          />
          </View>
          
        </View>
      </ScrollView>
    );
  }
}

const main = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    // alignItems: 'center',
    paddingHorizontal: 24,
    position: 'relative',
  },

  logoContainer: {
    position: 'absolute',
    top: 0,
    right: 0,
  },

  subheader: {
    fontSize: 24,
    fontWeight: '600',
    marginBottom: 8,
    marginTop: 16,
  },
  text: {
    marginLeft: 0,
    paddingTop: 8,
  },
  input: {
    marginLeft: 0
  },
})

const form = StyleSheet.create({
  group: {
    marginBottom: 32,
  }
})

