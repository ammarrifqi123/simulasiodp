import React from 'react';
import axios from 'axios';
import Reactotron from 'reactotron-react-native'
import { StyleSheet, Text, View, FlatList, TouchableOpacity, ActivityIndicator } from 'react-native';
import { Image, Icon } from 'react-native-elements';

import logoJenius from '../assets/logo-jenius.png';

export default class Recommendation extends React.Component {
  static navigationOptions = {
    title: 'Rekomendasi Investasi',
    headerStyle: {
      backgroundColor: '#20A4DC',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: 'bold',
    },
  };

  state = {
    tabActive: 1,
    listReksaDana: [],
    listSBN: []
  }

  componentWillMount() {
    this.getReksaDanaList();
    this.getSBN();
  }

  getSBN = () => {
    axios.get(`http://10.10.22.222:8080/investment/get_all_obligation`)
    .then(res => {
      Reactotron.log(res)
      this.setState({
        listSBN: res.data,
      })
    })
    .catch(err => {
      Reactotron.log(err)
    })
  }

  getReksaDanaList = () => {
    axios.get('http://10.10.22.222:8080/investment/get_all_reksadana')
    .then(res => {
      this.setState({
        listReksaDana: res.data,
      })
    })
    .catch(err => {
      Reactotron.log(err)
    })
  }


  handleChartTab = (val) => {
    this.setState({ tabActive: val })
  }

  renderReksaDana = (item) => {
    const growthClass = item._1YR.charAt(0) === '-' ? list.itemGrowthValueRed : list.itemGrowthValueGreen;

    return (
      <TouchableOpacity style={list.item} onPress={() => {}}>
        <View style={list.itemGrowth}>
          <Text style={list.itemGrowthLabel}>Growth</Text>
          <Text style={[list.itemGrowthValue, growthClass]}>{item._1YR}</Text>
        </View>
        <View style={{flexGrow: 2}}>
          <Text style={list.itemName}>{item.produk}</Text>
          <Text style={list.itemDesc}>{`${item.tipe} - ${item.pasar}`}</Text>
          <Text style={list.itemRating}>{`Rating: ${item.rating.substring(0, item.rating.length - 1)} / 100`}</Text>
        </View>
        <View>
          <Icon name="chevron-right" size={24} color="#20A4DC" style={{ marginRight: 8 }} />
        </View>
      </TouchableOpacity>
    )
  }

  renderSBN = (item) => {
    
    return (
      <TouchableOpacity style={list.item} onPress={() => {}}>
        <View style={list.itemGrowth}>
          <Text style={list.itemGrowthLabel}>Kupon</Text>
          <Text style={[list.itemGrowthValue, list.itemGrowthValueGreen]}>{`${item.couponCalculation} %`}</Text>
        </View>
        <View style={{flexGrow: 2}}>
          <Text style={list.itemName}>{item.couponSerial}</Text>
          <Text style={list.itemDesc}>Periode Penawaran : {`${item.offeringPeriod}-2019`}</Text>
          <Text style={list.itemRating}>{item.element}</Text>
        </View>
        <View>
          <Icon name="chevron-right" size={24} color="#20A4DC" style={{ marginRight: 8 }} />
        </View>
      </TouchableOpacity>
    )
  }

  render() {
    const { tabActive, listSBN, listReksaDana } = this.state;

    listSBN.forEach((item, index) => { item.key = index.toString() })
    listReksaDana.forEach((item, index) => { item.key = index.toString() })
    

    return (
      <View style={main.container}>
        <View style={main.logoContainer}>
          <Image style={{ height: 48 }} source={logoJenius} />
        </View>
        <View style={tab.tabRow}>
          <Text
            onPress={() => this.handleChartTab(1)}
            style={tabActive === 1 ? [tab.tabItem, tab.tabItemActive] : tab.tabItem}>Reksadana</Text>
          <Text
            onPress={() => this.handleChartTab(2)}
            style={tabActive === 2 ? [tab.tabItem, tab.tabItemActive] : tab.tabItem}>SBN</Text>
        </View>
        <View style={tab.tabContent}>
          {tabActive === 1 && (
            <View style={list.container}>
              {listReksaDana.length > 0
                ? <FlatList
                    data={listReksaDana}
                    renderItem={({item}) => this.renderReksaDana(item)}
                  />
                : <ActivityIndicator />
              }
            </View>
          )}
          {tabActive === 2 && (
            <View style={list.container}>
              {listSBN.length > 0
                ? <FlatList
                    data={listSBN}
                    renderItem={({item}) => this.renderSBN(item)}
                  />
                : <ActivityIndicator />
              }
            </View>
          )}
        </View>
      </View>
    );
  }
}

const main = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    position: 'relative',
  },

  logoContainer: {
    position: 'absolute',
    top: 0,
    right: 0,
  },

  subheader: {
    fontSize: 24,
    fontWeight: '600',
    marginBottom: 8,
    marginTop: 16,
  },
})


const tab = StyleSheet.create({
  tabRow: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    marginBottom: 8,
    marginTop: 48,
  },

  tabRemark: {
    paddingTop: 8,
    paddingBottom: 8,
    marginRight: 16,
    color: 'rgba(0,0,0,0.54)',
  },
  
  tabItem: {
    width: '50%',
    color: 'rgba(0,0,0,0.38)',
    fontWeight: '600',
    fontSize: 20,
    textAlign: 'center',
    backgroundColor: '#f4f4f4',
    padding: 16,
  },

  tabItemActive: {
    backgroundColor: '#fff',
    color: '#20A4DC',
    fontWeight: '600',
  },
})

const list = StyleSheet.create({
  container: {
    padding: 16,
  },
  item: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: 16,
    paddingHorizontal: 0,
    width: '100%',
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0,0,0,0.12)',
  },

  itemGrowth: {
    display: 'flex',
    minWidth: 100,
  },
  itemGrowthLabel: {
    color: 'rgba(0,0,0,0.38)',
  },
  itemGrowthValue: {
    fontSize: 24,
  },
  itemGrowthValueGreen: {
    color: '#88C425',
  },
  itemGrowthValueRed: {
    color: '#e26a00',
  },

  itemName: {
    fontSize: 16,
    fontWeight: '600',
  },
  itemDesc: {
    color: 'rgba(0,0,0,0.38)',
    marginTop: 4,
  },
  itemRating: {
    color: 'rgba(0,0,0,0.38)',
    marginTop: 16,
    fontStyle: 'italic'
  },
  itemElemen: {
    color: 'rgba(0,0,0,0.38)',
    marginTop: 16,
    marginRight: -50,
    
    fontStyle: 'italic'
  }
})
