import React, { Component } from 'react';
import Reactotron from 'reactotron-react-native'
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Dimensions,
} from 'react-native';
import axios from 'axios';
import { Image, Input, Button } from 'react-native-elements';
import { Font } from 'expo';
import Icon from 'react-native-vector-icons/FontAwesome';
import SplashScreen from '../SplashScreen'

const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;

import logoJenius from '../assets/logo.png';
const BG_IMAGE = require('../assets/images/bg_screen1.jpg');

export default class Login extends Component {
  static navigationOptions = {
    title: 'Activa',
  };

  state = {
    fontLoaded: false,
    email: '',
    email_valid: true,
    password: '',
    login_failed: false,
    showLoading: false,
    showSplashScreen: true,
  };

  login = () => {
    const { email, password } = this.state;

    this.setState({ showSplashScreen: true }, () => {
      setTimeout(() => {
        axios.post(`http://10.10.22.222:8080/user/login`, { cashTag: email, password: password })
        .then(res => {
          Reactotron.log(res)
          this.props.navigation.navigate('Survey', { cashTag: email })
        })
        .catch(err => {
          this.setState({ showSplashScreen: false })
          Reactotron.log(err)
        })
      }, 2000)
    })

  }

  componentWillMount() {
    setTimeout(() => {
      this.setState({ showSplashScreen: false });
    }, 2000)
  }

  async componentDidMount() {
    await Font.loadAsync({
      georgia: require('../assets/fonts/Georgia.ttf'),
      regular: require('../assets/fonts/Montserrat-Regular.ttf'),
      light: require('../assets/fonts/Montserrat-Light.ttf'),
      bold: require('../assets/fonts/Montserrat-Bold.ttf'),
    });

    this.setState({ fontLoaded: true });
  }

  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    return re.test(email);
  }

  submitLoginCredentials() {
    const { showLoading } = this.state;

    this.setState({
      showLoading: !showLoading,
    });
  }

  render() {
    const { email, password, email_valid, showLoading, showSplashScreen } = this.state;

    if (showSplashScreen) return <SplashScreen />

    return (
      <View style={styles.container}>
        <View style={styles.bgImage}>
          {this.state.fontLoaded ? (
            <View style={styles.loginView}>
              <View style={styles.loginTitle}>
                <View>
                  <Image
                    style={{
                      flex: 1,
                      width: 350,
                      height: 350,
                      resizeMode: 'contain'
                    }}
                    source={logoJenius} />
                </View>
              </View>
              <View style={styles.loginInput}>
                <Input
                  leftIcon={
                    <Icon
                      name="user-o"
                      color="#20A4DC"
                      size={24}
                    />
                  }
                  containerStyle={{ marginVertical: 12 }}
                  onChangeText={email => this.setState({ email })}
                  value={email}
                  inputStyle={{ marginLeft: 16, color: 'rgba(0,0,0,0.7)' }}
                  keyboardAppearance="light"
                  placeholder="Cashtag"
                  autoFocus={false}
                  autoCapitalize="none"
                  autoCorrect={false}
                  keyboardType="email-address"
                  returnKeyType="next"
                  ref={input => (this.emailInput = input)}
                  onSubmitEditing={() => {
                    this.setState({ email_valid: this.validateEmail(email) });
                    this.passwordInput.focus();
                  }}
                  blurOnSubmit={false}
                  placeholderTextColor='rgba(0,0,0,0.38)'
                  errorStyle={{ textAlign: 'center', fontSize: 12 }}
                  errorMessage={
                    email_valid ? null : 'Please enter a valid cashtag'
                  }
                  
                />
                <Input
                  leftIcon={
                    <Icon
                      name="lock"
                      color="#20A4DC"
                      size={24}
                    />
                  }
                  containerStyle={{ marginVertical: 12 }}
                  onChangeText={password => this.setState({ password })}
                  value={password}
                  inputStyle={{ marginLeft: 16, color: 'rgba(0,0,0,0.7)' }}
                  secureTextEntry={true}
                  keyboardAppearance="light"
                  placeholder="Password"
                  autoCapitalize="none"
                  autoCorrect={false}
                  keyboardType="default"
                  returnKeyType="done"
                  ref={input => (this.passwordInput = input)}
                  blurOnSubmit={true}
                  placeholderTextColor='rgba(0,0,0,0.38)'
                />
              </View>
              <Button
                  title="LOG IN"
                  clear
                  activeOpacity={0.5}
                  titleStyle={{ color: 'white', fontSize: 15 }}
                  containerStyle={{ marginTop: 10}}
                  buttonStyle={{backgroundColor: "#20A4DC"}}
                  onPress={this.login}
                />
              
            </View>
          ) : (
            <Text>Loading...</Text>
          )}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f4f4f4',
  },
  bgImage: {
    flex: 1,
    top: 0,
    left: 0,
    width: SCREEN_WIDTH,
    height: SCREEN_HEIGHT,
    justifyContent: 'center',
    alignItems: 'center',
  },
  loginView: {
    backgroundColor: 'transparent',
    width: 250,
    height: 400,
  },
  loginTitle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  travelText: {
    color: 'black',
    fontSize: 30,
    fontFamily: 'bold',
  },
  plusText: {
    color: 'black',
    fontSize: 30,
    fontFamily: 'regular',
  },
  loginInput: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  footerView: {
    marginTop: 15,
    flex: 0.5,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
