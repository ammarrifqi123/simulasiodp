import React from 'react';
import axios from 'axios';
import Reactotron from 'reactotron-react-native'
import Icon from 'react-native-vector-icons/Feather';
import Carousel from 'react-native-snap-carousel';
import RBSheet from "react-native-raw-bottom-sheet";
import moment from 'moment';
import { StyleSheet, Text, View, ActivityIndicator, Dimensions, ScrollView } from 'react-native';
import { Image, Avatar, Card, Button } from 'react-native-elements';
import { LineChart } from 'react-native-chart-kit';

import SplashScreen from '../SplashScreen';
import logoJenius from '../assets/logo-jenius.png';

export default class Dashboard extends React.Component {
  static navigationOptions = {
    title: 'Activa',
    headerStyle: {
      backgroundColor: '#20A4DC',
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: 'bold',
    },
  };

  state = {
    dog: null,
    tabActive: 1,
    chartData: [],
    userProfile: {
      cashTag: 'bobys',
      name: '-',
      savingAmount: 0,
      cashStatus: 'NOT_SAFE',
      profilImageURL: 'http://placehold.it/200x200',
    },
    userRecomendation: {},
    graphDisplayed: 0,
  }

  handleChartTab = (val) => {
    this.setState({ tabActive: val })
  }

  async componentWillMount() {
    this.getUserProfile();
    this.getUserRecomendation();
    this.getChartData();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.tabActive !== this.state.tabActive) {
      this.getChartData();
    }
  }

  getUserProfile = () => {
    const passedCashTag = this.props.navigation.getParam('cashTag', 'bobys')
    Reactotron.log(passedCashTag)

    axios.get(`http://10.10.22.222:8080/user/get_user/${passedCashTag}`)
    .then(res => {
      this.setState({
        userProfile: res.data,
      })
    })
    .catch(err => {
      Reactotron.log(err)
    })
  }

  getUserRecomendation = () => {
    const { userProfile } = this.state;

    axios.get(`http://10.10.22.222:8080/recomendation/get_saving_recomendation/${userProfile.cashTag}`)
    .then(res => {
      this.setState({
        userRecomendation: res.data
      })
    })
    .catch(err => {
      Reactotron.log(err)
    })
  }

  getChartData = () => {
    const { chartData, tabActive, userProfile } = this.state;

    let apiEndPoint = '';
    if (tabActive === 1) {
      apiEndPoint = 'transaction/get_transaction_monthly'
    } else {
      apiEndPoint = 'transaction/get_transaction_daily'
    }

    axios.get(`http://10.10.22.222:8080/${apiEndPoint}/${userProfile.cashTag}`)
    .then(res => {
      this.setState({
        chartData: res.data
      })
    })
    .catch(err => {
      Reactotron.log(err)
    })
  }
  
  
  formatMoney(integer) {
    return 'Rp. ' + integer.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
  }
  
  renderChartCarousel = ({item, index}) => {
    const mapAttr = {
      last: {
        backgroundColor: '#519548',
        backgroundGradientFrom: '#88C425',
        backgroundGradientTo: '#BEF202',
      },
      current: {
        backgroundColor: '#e26a00',
        backgroundGradientFrom: '#fb8c00',
        backgroundGradientTo: '#ffa726',
      }
    }

    const dataSetType = index === 0 ? 'balanceAmount' : 'cashOut'

    const { chartData } = this.state
    if (chartData.length < 1) return <ActivityIndicator />

    return (
      <LineChart
        data={{
          labels: chartData.map(item => {return item.timeVariable}),
          datasets: [{
            data: chartData.map(item => {return item[dataSetType]})
          }]
        }}
        width={Dimensions.get('window').width - 80}
        height={180}
        chartConfig={{
          backgroundColor: mapAttr[item].backgroundColor,
          backgroundGradientFrom: mapAttr[item].backgroundGradientFrom,
          backgroundGradientTo: mapAttr[item].backgroundGradientTo,
          decimalPlaces: 0,
          color: () => '#f4f4f4',
          style: {
            borderRadius: 8
          }
        }}
        style={{
          marginVertical: 8,
          borderRadius: 8
        }}
      />
    )
  }

  render() {
    const { tabActive, chartData, userProfile, userRecomendation, graphDisplayed } = this.state;

    const mapStatus = {
      SAFE: {
        icon: "check",
        iconColor: "#88C425",
        remark: "Aman",
      },
      NOT_SAFE: {
        icon: "x-circle",
        iconColor: "#e26a00",
        remark: "Tidak Aman",
      }
    }

    return (
      <ScrollView>
        <View style={main.container}>
          <View style={main.logoContainer}>
            <Image style={{ height: 48 }} source={logoJenius} />
          </View>
          <View style={profile.container}>
            <Avatar
              rounded
              size="large"
              source={{
                uri: userProfile.profilImageURL || 'http://placehold.it/200x200',
              }}
            />
            <View style={profile.text}>
              <Text style={profile.name}>{userProfile.name}</Text>
              <Text style={profile.cashtag}>{`$${userProfile.cashTag}`}</Text>
            </View>
          </View>
          <View style={statusCard.container}>
            <View style={statusCard.card}>
              <View style={statusCard.cardFlex}>
                <View style={statusCard.cardSectionLeft}>
                  <View style={{ display: 'flex', flexDirection: 'row' }}>
                    <Icon name={mapStatus[userProfile.cashStatus].icon} size={20} color={mapStatus[userProfile.cashStatus].iconColor} style={{ marginRight: 8 }} />
                    <Text style={statusCard.value}>{mapStatus[userProfile.cashStatus].remark}</Text>
                  </View>
                  <Text style={statusCard.label}>Status Kas</Text>
                </View>
                <View style={statusCard.cardSectionRight}>
                  <Text style={statusCard.value}>{this.formatMoney(userProfile.savingAmount)}</Text>
                  <Text style={statusCard.label}>Dana Aman</Text>
                </View>
              </View>
              <View style={statusCard.cardLink}>
                <Text style={statusCard.cardLinkText} onPress={() => this.SavingSheet.open()}>Lihat Detail</Text>
              </View>
            </View>
          </View>
          <View style={chart.container}>
            <View style={{ display: 'flex', flexDirection: 'row', alignItems: 'baseline' }}>
              <Text style={main.subheader}>Cashflow</Text>
              <Text style={main.subheader2}>{graphDisplayed === 0 ? ". SALDO" : ". PENGELUARAN"}</Text>
            </View>
            <View style={tabTimeSpan.tabRow}>
              <Text style={tabTimeSpan.tabRemark}>Tampilkan data:</Text>
              <Text
                onPress={() => this.handleChartTab(1)}
                style={tabActive === 1 ? [tabTimeSpan.tabItem, tabTimeSpan.tabItemActive] : tabTimeSpan.tabItem}>3bln</Text>
              <Text
                onPress={() => this.handleChartTab(2)}
                style={tabActive === 2 ? [tabTimeSpan.tabItem, tabTimeSpan.tabItemActive] : tabTimeSpan.tabItem}>1bln</Text>
            </View>
            <View style={carousel.container}>
              <Carousel
                ref={c => { this.carousel = c; }}
                data={['current', 'last']}
                renderItem={this.renderChartCarousel}
                sliderWidth={Dimensions.get('window').width - 48}
                itemWidth={Dimensions.get('window').width - 80}
                activeSlideAlignment='start'
                sliderHeight={180}
                onSnapToItem={(index) => this.setState({ graphDisplayed: index })}
              />
            </View>
            
          </View>
          <Text style={suggestion.remark}>
            Kami telah membuat rekomendasi investasi untuk Anda sesuai cashflow dan profil keuangan Anda.
          </Text>
          <Button
            onPress={() => userProfile.cashStatus === "SAFE" ? this.props.navigation.navigate('Recommendation') : this.WarningSheet.open()}
            title="Lihat Rekomendasi Investasi"
            buttonStyle={{
              width: Dimensions.get('window').width - 48,
              height: 48,
              backgroundColor: "#20A4DC",
            }}
          />
          <RBSheet
            ref={ref => { this.SavingSheet = ref }}
            height={300}
            duration={250}
            customStyles={{
              container: {
                alignItems: "flex-start",
                borderTopRightRadius: 16,
                borderTopLeftRadius: 16,
                padding: 16,
              }
            }}>
            <Text style={main.subheader}>Detail Keuangan</Text>
            <View style={detail.container}>
              <View style={detail.section}>
                <Text style={detail.label}>Pengeluaran bulan ini</Text>
                <Text style={detail.value}>{this.formatMoney(Math.floor(userRecomendation.outcomeThisMonth) || 0)}</Text>
              </View>
              <View style={detail.section}>
                <Text style={detail.label}>Saving bulan ini</Text>
                <Text style={detail.value}>{this.formatMoney(Math.floor(userRecomendation.savingThisMonth) || 0)}</Text>
              </View>
              <View style={detail.section}>
                <Text style={detail.label}>Rekomendasi Saving</Text>
                <Text style={detail.value}>{this.formatMoney(Math.floor(userRecomendation.savingRecomendation) || 0)}</Text>
              </View>
              <View style={detail.section}>
                <Text style={detail.label}>Rekomendasi Uang Investasi</Text>
                <Text style={detail.value}>{this.formatMoney(Math.floor(userRecomendation.investmentRecomendation) || 0)}</Text>
              </View>
            </View>
          </RBSheet>
          <RBSheet
            ref={ref => { this.WarningSheet = ref }}
            height={280}
            duration={250}
            customStyles={{
              container: {
                alignItems: "flex-start",
                borderTopRightRadius: 16,
                borderTopLeftRadius: 16,
                padding: 16,
              }
            }}>
            <View>
              <Text style={main.subheader}>{`Hi, ${userProfile.name}`}</Text>
              <Text style={main.paragraph}>Saat ini kondisi keuangan kamu sedang dibawah batas aman. Kamu bisa mulai berhemat dan mengurangi pengeluaran untuk mendapatkan keuangan yang sehat. Kami akan buatkan rekomendasi investasi apabila kondisi keuangan kamu sudah stabil.</Text>
              <Button
                onPress={() => this.WarningSheet.close()}
                title="OK"
                buttonStyle={{
                  width: Dimensions.get('window').width - 48,
                  height: 48,
                  backgroundColor: "#20A4DC",
                  marginTop: 32,
                }}
              />
            </View>
          </RBSheet>
        </View>
      </ScrollView>
    );
  }
}

const main = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    position: 'relative',
  },

  logoContainer: {
    top: 0,
    right: 0,
  },

  subheader: {
    fontSize: 24,
    fontWeight: '600',
    marginBottom: 8,
    marginTop: 16,
  },

  subheader2: {
    fontSize: 16,
    fontWeight: '600',
    marginBottom: 8,
    marginLeft: 4,
    color: 'rgba(0,0,0,0.38)'
  },

  paragraph: {
    color: 'rgba(0,0,0,0.54)',
    fontSize: 14,
    lineHeight: 20,
  },
})


const profile = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'row',
    padding: 24,
    paddingBottom: 80,
    backgroundColor: '#20A4DC',
    width: '100%',
    color: '#fff',
  },

  text: {
    marginLeft: 24,
    paddingTop: 8,
  },

  name: {
    color: '#fff',
    fontSize: 24,
  },

  cashtag: {
    color: '#f4f4f4',
    fontSize: 16,
  },
})

const statusCard = StyleSheet.create({
  container: {
    paddingLeft: 24,
    paddingRight: 24,
    marginTop: -56,
    textAlign: 'center',
  },
  
  card: {
    marginLeft: 24,
    marginRight: 24,
    borderRadius: 8,
    backgroundColor: '#fff',
    width: '100%',
    elevation: 1,
    shadowColor: 'rgba(0,0,0,0.12)',
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 5,
    shadowOpacity: 1.0,
  },
  cardLink: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'row',
    width: '100%',
  },
  cardLinkText: {
    color: '#20A4DC',
    width: '100%',
    padding: 12,
    textAlign: 'center'
  },
  cardFlex: {
    display: 'flex',
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0,0,0,0.12)',
  },

  cardSectionLeft: {
    padding: 16,
    width: '45%',
    borderRightWidth: 1,
    borderRightColor: 'rgba(0,0,0,0.12)',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  cardSectionRight: {
    padding: 16,
    width: '55%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  label: {
    color: 'rgba(0,0,0,0.38)',
    fontSize: 14,
    marginTop: 4,
  },
  value: {
    color: 'rgba(0,0,0,0.7)',
    fontSize: 20,
    fontWeight: '600',
  },
})

const chart = StyleSheet.create({
  container: {
    margin: 24,
  },
})

const tabGraph = StyleSheet.create({
  tabRow: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginBottom: 8,
  },
  
  tabItem: {
    paddingTop: 8,
    paddingBottom: 8,
    marginRight: 16,
    color: 'rgba(0,0,0,0.38)',
  },

  tabItemActive: {
    color: '#20A4DC',
    fontWeight: '600',
  },

})

const tabTimeSpan = StyleSheet.create({
  tabRow: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    marginBottom: 8,
  },

  tabRemark: {
    paddingTop: 8,
    paddingBottom: 8,
    marginRight: 16,
    color: 'rgba(0,0,0,0.54)',
  },
  
  tabItem: {
    paddingTop: 8,
    paddingBottom: 8,
    marginRight: 16,
    color: 'rgba(0,0,0,0.38)',
  },

  tabItemActive: {
    color: '#20A4DC',
    fontWeight: '600',
  },
})

const cta = StyleSheet.create({
  container: {
    borderRadius: 8,
    height: 48,
  },
  button: {
    color: '#20A4DC',
    borderWidth: 2,
    borderColor: '#20A4DC',
  }
})

const carousel = StyleSheet.create({
  container: {
    height: 240,
  }
})

const suggestion = StyleSheet.create({
  remark: {
    textAlign: 'center',
    fontSize: 12,
    paddingHorizontal: 24,
    marginBottom: 16,
    color: 'rgba(0,0,0,0.54)',
  }
})

const detail = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center',
    justifyContent: 'space-around',
    marginTop: 8,
  },
  section: {
    width: '50%',
    marginBottom: 24,
  },
  label: {
    color: 'rgba(0,0,0,0.38)',
    fontSize: 14,
    marginBottom: 4,
  },
  value: {
    color: 'rgba(0,0,0,0.7)',
    fontSize: 20,
    fontWeight: '600',
  },
})

