import React from 'react';
import { StyleSheet, Text, View, ActivityIndicator } from 'react-native';
import { Image } from 'react-native-elements';
import { Dropdown } from 'react-native-material-dropdown';

import logoJeniusLg from './assets/logo-jenius-lg.png';


export default class SplashScreen extends React.Component {
  render() {
    return (
      <View style={main.container}>
        <Image style={{ width: 300 }} source={logoJeniusLg} />
        <ActivityIndicator />
      </View>
    );
  }
}

const main = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
})

