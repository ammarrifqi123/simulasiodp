if(__DEV__) {
  import('./ReactotronConfig').then(() => console.log('Reactotron Configured'))
}

import React from 'react';
import {createStackNavigator, createAppContainer} from 'react-navigation';
import Dashboard from './views/Dashboard';
import Recommendation from './views/Recommendation';
import Survey from './views/Survey';
import Login from './views/Login';

const MainNavigator = createStackNavigator({
  Login: {screen: Login},
  Survey: {screen: Survey},
  Dashboard: {screen: Dashboard},
  Recommendation: {screen: Recommendation},
});

const App = createAppContainer(MainNavigator);

export default App;
