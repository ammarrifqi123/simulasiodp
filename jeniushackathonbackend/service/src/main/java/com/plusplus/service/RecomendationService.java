package com.plusplus.service;

import com.plusplus.vo.models.Obligation;
import com.plusplus.vo.models.SavingRecomendation;

/**
 * Created by Vostro on 23/02/2019.
 */
public interface RecomendationService {
    public SavingRecomendation getSavingRecomendation(String cashTag);
    public Obligation getObligationRecomendation(String cashTag);
}
