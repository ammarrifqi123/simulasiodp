package com.plusplus.service;

import com.plusplus.repository.UMRRepository;
import com.plusplus.vo.models.UMR;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Vostro on 24/02/2019.
 */
@Service
public interface DataService {
    public List<UMR> getAllLocation();
    public void addAllLocation(List<UMR> umrList);
}
