package com.plusplus.service;

import com.plusplus.vo.models.User;

import java.util.List;

/**
 * Created by Vostro on 23/02/2019.
 */
public interface UserService {
    public void saveUser(User user);
    public void saveUserList(List<User> userList);
    public List<User> getUserList();
    public User getUser(String cashTag);
    public boolean login(String cashTag, String password);
}
