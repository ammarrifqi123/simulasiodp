package com.plusplus.service;

import com.plusplus.vo.models.Survey;

import java.util.List;

/**
 * Created by Vostro on 24/02/2019.
 */
public interface SurveyService {
    public void addSurvey(Survey survey);
    public List<Survey> getAllSurvey();
}
