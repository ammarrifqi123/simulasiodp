package com.plusplus.service;

import com.plusplus.vo.models.Obligation;
import com.plusplus.vo.models.Reksadana;

import java.util.List;

/**
 * Created by Vostro on 24/02/2019.
 */
public interface InvestmentService {
    public void addObligationList(List<Obligation> obligations);
    public Obligation getObligationByCouponSerial(String couponSerial);
    public List<Obligation> getAllObligation();
    public void addReksadanaList(List<Reksadana> reksadanas);
    public List<Reksadana> getAllReksadana();
}
