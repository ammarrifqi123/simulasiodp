package com.plusplus.service;


import com.plusplus.vo.models.Transaction;
import com.plusplus.vo.models.TransactionResponse;

import java.util.List;

/**
 * Created by Vostro on 23/02/2019.
 */
public interface TransactionService {
    public void saveTransaction(Transaction transaction);
    public void saveTransactionList(List<Transaction> transaction);
    public void generateTransactionSampling();
    public List<TransactionResponse> getTransactionMonthly(String cashTag);
    public List<TransactionResponse> getTransactionDaily(String cashTag);
}
