package com.plusplus.serviceimpl.services;

import com.plusplus.repository.ObligationRepository;
import com.plusplus.repository.ReksadanaRepository;
import com.plusplus.service.InvestmentService;
import com.plusplus.serviceimpl.utils.MapperService;
import com.plusplus.vo.models.Obligation;
import com.plusplus.vo.models.Reksadana;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Vostro on 24/02/2019.
 */
@Service
public class InvestmentServiceImpl implements InvestmentService{
    @Autowired
    private ObligationRepository obligationRepository;
    @Autowired
    private ReksadanaRepository reksadanaRepository;
    @Autowired
    private DozerBeanMapper mapper;
    @Autowired
    private MapperService mapperService;

    @Override
    public void addObligationList(List<Obligation> obligations) {
        obligationRepository.saveAll(mapperService.map(mapper, obligations, com.plusplus.entity.models.Obligation.class));
    }

    @Override
    public Obligation getObligationByCouponSerial(String couponSerial) {
        return mapper.map(obligationRepository.findByCouponSerial(couponSerial), Obligation.class);
    }

    @Override
    public List<Obligation> getAllObligation() {
        return mapperService.map(mapper, obligationRepository.findAll(), Obligation.class);
    }

    @Override
    public void addReksadanaList(List<Reksadana> reksadanas) {
        reksadanaRepository.saveAll(mapperService.map(mapper, reksadanas, com.plusplus.entity.models.Reksadana.class));
    }

    @Override
    public List<Reksadana> getAllReksadana() {
        return mapperService.map(mapper, reksadanaRepository.findAll(), Reksadana.class);
    }
}
