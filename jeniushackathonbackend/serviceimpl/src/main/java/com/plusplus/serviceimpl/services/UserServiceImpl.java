package com.plusplus.serviceimpl.services;

import com.plusplus.repository.TransactionRepository;
import com.plusplus.repository.UserRepository;
import com.plusplus.service.UserService;
import com.plusplus.serviceimpl.utils.MapperService;
import com.plusplus.vo.models.User;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Vostro on 23/02/2019.
 */
@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private DozerBeanMapper mapper;
    @Autowired
    private MapperService mapperService;

    @Override
    public void saveUser(User user) {
        userRepository.save(mapper.map(user, com.plusplus.entity.models.User.class));
    }

    @Override
    public void saveUserList(List<User> userList) {
        userRepository.saveAll(mapperService.map(mapper, userList, com.plusplus.entity.models.User.class));
    }

    @Override
    public List<User> getUserList() {
        List<com.plusplus.entity.models.User> userList = userRepository.findAll();
        List<User> users = mapperService.map(mapper, userList, User.class);
        return users;
    }

    @Override
    public User getUser(String cashTag) {
        return mapper.map(userRepository.findUserByCashTag(cashTag), User.class);
    }

    @Override
    public boolean login(String cashTag, String password) {
        com.plusplus.entity.models.User user = userRepository.findUserByCashTag(cashTag);
        if(password.equals(user.getPassword())){
            return true;
        }else {
            return false;
        }
    }


}
