package com.plusplus.serviceimpl.services;

import com.plusplus.entity.enums.TransactionType;
import com.plusplus.repository.TransactionRepository;
import com.plusplus.repository.UserRepository;
import com.plusplus.service.TransactionService;
import com.plusplus.serviceimpl.utils.DateComparator;
import com.plusplus.serviceimpl.utils.DateService;
import com.plusplus.serviceimpl.utils.GeneralHelper;
import com.plusplus.serviceimpl.utils.MapperService;
import com.plusplus.vo.models.Transaction;
import com.plusplus.vo.models.TransactionResponse;
import com.plusplus.vo.models.User;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by Vostro on 23/02/2019.
 */
@Service
public class TransactionServiceImpl implements TransactionService {
    @Autowired
    private UserServiceImpl userService;
    @Autowired
    private TransactionRepository transactionRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private DozerBeanMapper mapper;
    @Autowired
    private MapperService mapperService;

    private List<com.plusplus.entity.models.Transaction> constructTransactionHighFreq(User user, String month, String year, Double baseSalary){
        List<com.plusplus.entity.models.Transaction> transactions = new ArrayList<>();
        Double balanceAmount = user.getSavingAmount() + baseSalary;

        com.plusplus.entity.models.Transaction transaction = com.plusplus.entity.models.Transaction.builder()
                .cashTag(user.getCashTag())
                .transactionType(TransactionType.DEBIT)
                .transactionAmount(baseSalary)
                .lastBalanceAmount(balanceAmount)
                .transactionDate(DateService.parseDate(year+"-"+month+"-25"))
                .build();
        transactions.add(transaction);

        for(int i=1;i<=28;i+=2){
            Date transactionDate;
            Double transactionAmount = new Double(ThreadLocalRandom.current().nextInt(100000, 300000));
            balanceAmount = balanceAmount-transactionAmount;
            if(i<10) {
                transactionDate = DateService.parseDate(year + "-" + month + "-0" + i);
            }else{
                transactionDate = DateService.parseDate(year + "-" + month + "-" + i);
            }
            com.plusplus.entity.models.Transaction transactionDaily = com.plusplus.entity.models.Transaction.builder()
                    .cashTag(user.getCashTag())
                    .transactionType(TransactionType.KREDIT)
                    .transactionAmount(transactionAmount)
                    .lastBalanceAmount(balanceAmount)
                    .transactionDate(transactionDate)
                    .build();
            transactions.add(transactionDaily);
        }
        user.setSavingAmount(balanceAmount);
        return transactions;
    }

    private List<com.plusplus.entity.models.Transaction> constructTransactionLowFreq(User user, String month, String year, Double baseSalary){
        List<com.plusplus.entity.models.Transaction> transactions = new ArrayList<>();
        Double balanceAmount = user.getSavingAmount() + baseSalary;

        com.plusplus.entity.models.Transaction transaction = com.plusplus.entity.models.Transaction.builder()
                .cashTag(user.getCashTag())
                .transactionType(TransactionType.DEBIT)
                .transactionAmount(baseSalary)
                .lastBalanceAmount(balanceAmount)
                .transactionDate(DateService.parseDate(year+"-"+month+"-25"))
                .build();
        transactions.add(transaction);
        for(int i=1;i<=28;i+=5){
            Date transactionDate;
            Double transactionAmount = new Double(ThreadLocalRandom.current().nextInt(100000, 300000));
            balanceAmount = balanceAmount-transactionAmount;
            if(i<10) {
                transactionDate = DateService.parseDate(year + "-" + month + "-0" + i);
            }else{
                transactionDate = DateService.parseDate(year + "-" + month + "-" + i);
            }
            com.plusplus.entity.models.Transaction transactionDaily = com.plusplus.entity.models.Transaction.builder()
                    .cashTag(user.getCashTag())
                    .transactionType(TransactionType.KREDIT)
                    .transactionAmount(transactionAmount)
                    .lastBalanceAmount(balanceAmount)
                    .transactionDate(transactionDate)
                    .build();
            transactions.add(transactionDaily);
        }
        user.setSavingAmount(balanceAmount);
        return transactions;
    }

    private void updeteUserSavingAmount(String cashTag, String startDate, String endDate){
        List<com.plusplus.entity.models.Transaction> transactions = transactionRepository.findByCashTagAndTransactionDateBetween(
                cashTag, DateService.parseDate(startDate), DateService.parseDate(endDate));
        Collections.sort(transactions, new DateComparator());
        com.plusplus.entity.models.User user = userRepository.findUserByCashTag(cashTag);
        user.setSavingAmount(transactions.get(transactions.size()-1).getLastBalanceAmount());
        userRepository.save(user);
    }

    private TransactionResponse getTransactionResponseMonthly(String cashTag, String startDate, String endDate){
        Double cashOutAverage= new Double(0);
        Double balanceAverage= new Double(0);
        Integer totalCreditTransaction = 0;
        String month = DateService.getMonthFromDate(startDate);
        List<com.plusplus.entity.models.Transaction> transactions = transactionRepository.findByCashTagAndTransactionDateBetween(
                cashTag, DateService.parseDate(startDate), DateService.parseDate(endDate));
        for (com.plusplus.entity.models.Transaction transaction : transactions) {
            if(transaction.getTransactionType().equals(TransactionType.KREDIT)){
                cashOutAverage += transaction.getTransactionAmount();
                balanceAverage += transaction.getLastBalanceAmount();
                totalCreditTransaction += 1;
            }
        }
        cashOutAverage = cashOutAverage/totalCreditTransaction;
        balanceAverage = balanceAverage/totalCreditTransaction;
        return TransactionResponse.builder()
                .balanceAmount(balanceAverage)
                .cashOut(cashOutAverage)
                .timeVariable(month)
                .build();
    }

    private List<TransactionResponse> getTransactionResponseDaily(String cashTag, String startDate, String endDate){
        List<com.plusplus.entity.models.Transaction> transactions = transactionRepository.findByCashTagAndTransactionDateBetween(
                cashTag, DateService.parseDate(startDate), DateService.parseDate(endDate));
        Collections.sort(transactions, new DateComparator());
        List<TransactionResponse> transactionResponses = new ArrayList<>();

        for (int i = 0; i < transactions.size(); i++) {
            if(GeneralHelper.isNotFirstIndexAndNotLastIndex(transactions, i) &&
                    transactions.get(i).getTransactionType().equals(TransactionType.KREDIT)){
                TransactionResponse transactionResponse = TransactionResponse.builder()
                        .balanceAmount(transactions.get(i).getLastBalanceAmount())
                        .cashOut(transactions.get(i).getTransactionAmount())
                        .timeVariable(".")
                        .build();
                transactionResponses.add(transactionResponse);
            }
            else if(!GeneralHelper.isNotFirstIndexAndNotLastIndex(transactions, i) &&
                    transactions.get(i).getTransactionType().equals(TransactionType.KREDIT)){
                TransactionResponse transactionResponse = TransactionResponse.builder()
                        .balanceAmount(transactions.get(i).getLastBalanceAmount())
                        .cashOut(transactions.get(i).getTransactionAmount())
                        .timeVariable(transactions.get(i).getTransactionDate().toString())
                        .build();
                transactionResponses.add(transactionResponse);
            }
        }
        return transactionResponses;
    }

    @Override
    public void saveTransaction(Transaction transaction) {
        transactionRepository.save(mapper.map(transaction, com.plusplus.entity.models.Transaction.class));
    }

    @Override
    public void saveTransactionList(List<Transaction> transactionList) {
        transactionRepository.saveAll(mapperService.map(mapper, transactionList, com.plusplus.entity.models.Transaction.class));
    }

    @Override
    public void generateTransactionSampling() {
        userService.getUserList().forEach(
                user -> {
                    int generateParameter = ThreadLocalRandom.current().nextInt(0, 2);
                    if(generateParameter == 0) {
                        Double salaryAmount = new Double(ThreadLocalRandom.current().nextInt(6000000, 20000000));
                        transactionRepository.saveAll(constructTransactionHighFreq(user, "12", "2018", salaryAmount));
                        transactionRepository.saveAll(constructTransactionHighFreq(user, "01", "2019", salaryAmount));
                        transactionRepository.saveAll(constructTransactionHighFreq(user, "02", "2019", salaryAmount));
                    }
                    else {
                        Double salaryAmount = new Double(ThreadLocalRandom.current().nextInt(3900000, 6000000));
                        transactionRepository.saveAll(constructTransactionLowFreq(user, "12", "2018", salaryAmount));
                        transactionRepository.saveAll(constructTransactionLowFreq(user, "01", "2019", salaryAmount));
                        transactionRepository.saveAll(constructTransactionLowFreq(user, "02", "2019", salaryAmount));
                    }
                }
        );
    }

    @Override
    public List<TransactionResponse> getTransactionMonthly(String cashTag) {
        List<TransactionResponse> transactionResponses = new ArrayList<>();
        transactionResponses.add(getTransactionResponseMonthly(cashTag, "2018-12-01", "2018-12-31"));
        transactionResponses.add(getTransactionResponseMonthly(cashTag, "2019-01-01", "2019-01-31"));
        transactionResponses.add(getTransactionResponseMonthly(cashTag, "2019-02-01", "2019-02-28"));
        return transactionResponses;
    }

    @Override
    public List<TransactionResponse> getTransactionDaily(String cashTag) {
        return getTransactionResponseDaily(cashTag, "2019-02-01", "2019-02-28");
    }
}
