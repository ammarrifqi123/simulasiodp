package com.plusplus.serviceimpl.utils;

import com.plusplus.entity.models.Transaction;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Vostro on 23/02/2019.
 */
@Service
public class GeneralHelper {
    public static boolean isNotFirstIndexAndNotLastIndex(List<Transaction> source, int i){
        if(i!=0 && i!= source.size()-1){
            return true;
        }else {
            return false;
        }
    }
}
