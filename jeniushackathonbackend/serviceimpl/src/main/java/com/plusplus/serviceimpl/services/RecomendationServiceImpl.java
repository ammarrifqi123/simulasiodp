package com.plusplus.serviceimpl.services;

import com.plusplus.entity.enums.TransactionType;
import com.plusplus.entity.models.Transaction;
import com.plusplus.repository.TransactionRepository;
import com.plusplus.service.RecomendationService;
import com.plusplus.serviceimpl.utils.DateService;
import com.plusplus.vo.models.Obligation;
import com.plusplus.vo.models.SavingRecomendation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Vostro on 23/02/2019.
 */
@Service
public class RecomendationServiceImpl implements RecomendationService {
    @Autowired
    TransactionRepository transactionRepository;

    @Override
    public SavingRecomendation getSavingRecomendation(String cashTag) {
        List<Transaction> transactions = transactionRepository.findByCashTagAndTransactionDateBetween(
                cashTag, DateService.parseDate("2019-02-01"), DateService.parseDate("2019-02-28"));
        Double debitAmount = new Double(0);
        Double creditAmount = new Double(0);
        for (Transaction transaction : transactions) {
            if(transaction.getTransactionType().equals(TransactionType.KREDIT)){
                creditAmount += transaction.getTransactionAmount();
            }else {
                debitAmount += transaction.getTransactionAmount();
            }
        }

        Double savingAmount = debitAmount-creditAmount;
        Double investmentRecomendationAmount = savingAmount-3900000>0?savingAmount-3900000:0;

        SavingRecomendation savingRecomendation = SavingRecomendation.builder()
                .savingThisMonth(debitAmount-creditAmount)
                .outcomeThisMonth(creditAmount)
                .savingRecomendation(0.3 * debitAmount)
                .investmentRecomendation(investmentRecomendationAmount)
                .build();
        return savingRecomendation;

    }

    @Override
    public Obligation getObligationRecomendation(String cashTag) {
        return null;
    }

}
