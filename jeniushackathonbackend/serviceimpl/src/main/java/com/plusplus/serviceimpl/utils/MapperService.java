package com.plusplus.serviceimpl.utils;

import org.dozer.Mapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vostro on 23/02/2019.
 */
@Service
public class MapperService {
    public static <T, U> List<U> map(final Mapper mapper, final List<T> source, final Class<U> destType) {
        final List<U> dest = new ArrayList<>();
        for (T element : source) {
            dest.add(mapper.map(element, destType));
        }
        return dest;
    }
}
