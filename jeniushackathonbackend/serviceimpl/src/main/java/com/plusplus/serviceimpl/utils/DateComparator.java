package com.plusplus.serviceimpl.utils;

import com.plusplus.entity.models.Transaction;
import com.plusplus.vo.models.TransactionResponse;

import java.util.Comparator;

/**
 * Created by Vostro on 23/02/2019.
 */
public class DateComparator implements Comparator<Transaction> {
    @Override
    public int compare(Transaction t1, Transaction t2) {
        return t1.getTransactionDate().compareTo(t2.getTransactionDate());
    }
}
