package com.plusplus.serviceimpl.utils;

import org.springframework.stereotype.Service;

import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Vostro on 23/02/2019.
 */
@Service
public class DateService {
    public static Date parseDate(String date) {
        try {
            return new SimpleDateFormat("yyyy-MM-dd").parse(date);
        } catch (ParseException e) {
            return null;
        }
    }
    public static String getMonthFromDate(String date){
        String [] dateParts = date.split("-");
        String month = new DateFormatSymbols().getMonths()[Integer.parseInt(dateParts[1])-1];
        return month;
    }
}
