package com.plusplus.serviceimpl.services;

import com.plusplus.repository.SurveyRepository;
import com.plusplus.service.SurveyService;
import com.plusplus.serviceimpl.utils.MapperService;
import com.plusplus.vo.models.Survey;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Vostro on 24/02/2019.
 */
@Service
public class SurveyServiceImpl implements SurveyService {
    @Autowired
    private SurveyRepository surveyRepository;
    @Autowired
    private DozerBeanMapper mapper;
    @Autowired
    private MapperService mapperService;

    @Override
    public void addSurvey(Survey survey) {
        surveyRepository.save(mapper.map(survey, com.plusplus.entity.models.Survey.class));
    }

    @Override
    public List<Survey> getAllSurvey() {
        return mapperService.map(mapper, surveyRepository.findAll(), Survey.class);
    }
}
