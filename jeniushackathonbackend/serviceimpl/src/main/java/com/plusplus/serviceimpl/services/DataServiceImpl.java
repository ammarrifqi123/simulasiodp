package com.plusplus.serviceimpl.services;

import com.plusplus.repository.UMRRepository;
import com.plusplus.service.DataService;
import com.plusplus.serviceimpl.utils.MapperService;
import com.plusplus.vo.models.UMR;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Vostro on 24/02/2019.
 */
@Service
public class DataServiceImpl implements DataService {
    @Autowired
    private UMRRepository umrRepository;
    @Autowired
    private DozerBeanMapper mapper;
    @Autowired
    private MapperService mapperService;

    @Override
    public List<UMR> getAllLocation() {
        return mapperService.map(mapper, umrRepository.findAll(), UMR.class);
    }

    @Override
    public void addAllLocation(List<UMR> umrList) {
        umrRepository.saveAll(mapperService.map(mapper, umrList, com.plusplus.entity.models.UMR.class));
    }
}
