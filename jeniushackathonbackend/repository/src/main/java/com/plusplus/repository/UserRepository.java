package com.plusplus.repository;

import com.plusplus.entity.models.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Vostro on 23/02/2019.
 */
@Repository
public interface UserRepository  extends MongoRepository<User, String> {
    public User findUserByCashTag(String cashTag);
}
