package com.plusplus.repository;

import com.plusplus.entity.models.Reksadana;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by Vostro on 24/02/2019.
 */
public interface ReksadanaRepository extends MongoRepository<Reksadana, String> {
}
