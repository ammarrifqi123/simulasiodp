package com.plusplus.repository;

import com.plusplus.entity.models.Survey;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Vostro on 24/02/2019.
 */
@Repository
public interface SurveyRepository extends MongoRepository<Survey, String> {
}
