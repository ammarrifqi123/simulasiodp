package com.plusplus.repository;

import com.plusplus.entity.models.Obligation;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Vostro on 24/02/2019.
 */
@Repository
public interface ObligationRepository extends MongoRepository<Obligation, String> {
    public Obligation findByCouponSerial(String couponSerial);
}
