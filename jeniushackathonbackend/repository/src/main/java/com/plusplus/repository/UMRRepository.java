package com.plusplus.repository;

import com.plusplus.entity.models.UMR;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Vostro on 24/02/2019.
 */
@Repository
public interface UMRRepository extends MongoRepository<UMR, String> {
    public UMR findUserByLocationId(String locationId);
}
