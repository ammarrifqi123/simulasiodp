package com.plusplus.repository;

import com.plusplus.entity.models.Transaction;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Date;
import java.util.List;

/**
 * Created by Vostro on 23/02/2019.
 */
public interface TransactionRepository extends MongoRepository<Transaction, String> {
    public List<Transaction> findByCashTag(String cashTag);
    public List<Transaction> findByCashTagAndTransactionDateBetween(String cashTag, Date startDate, Date endDate);
}
