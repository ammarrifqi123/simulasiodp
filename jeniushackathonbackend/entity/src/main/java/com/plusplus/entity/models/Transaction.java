package com.plusplus.entity.models;

import com.plusplus.entity.enums.TransactionType;
import lombok.*;

import java.util.Date;

/**
 * Created by Vostro on 23/02/2019.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class Transaction {
    private String cashTag;
    private Date transactionDate;
    private TransactionType transactionType;
    private Double transactionAmount;
    private String transactionDesc;
    private Double lastBalanceAmount;
}
