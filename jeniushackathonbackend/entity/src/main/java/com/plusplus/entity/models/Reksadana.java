package com.plusplus.entity.models;

import lombok.*;

/**
 * Created by Vostro on 24/02/2019.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class Reksadana {
    private String Produk;
    private String Pasar;
    private String Tipe;
    private String LAST_NAV;
    private String _1D;
    private String _3D;
    private String _1M;
    private String YTD;
    private String _1YR;
    private String _3YR;
    private String Rating;
    private String SHARPE_Ratio;
    private String DDOWN;
    private String Dana_Kelolaan;
}
