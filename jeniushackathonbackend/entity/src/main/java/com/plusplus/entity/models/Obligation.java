package com.plusplus.entity.models;

import lombok.*;

/**
 * Created by Vostro on 24/02/2019.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class Obligation {
    private String couponSerial;
    private String offeringPeriod;
    private int tenorMonth;
    private Double minimumOrder;
    private Double maximumOrder;
    private Double couponCalculation;
    private String element;
}
