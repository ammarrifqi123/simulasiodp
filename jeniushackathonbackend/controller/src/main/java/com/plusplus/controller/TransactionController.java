package com.plusplus.controller;

import com.plusplus.serviceimpl.services.TransactionServiceImpl;
import com.plusplus.serviceimpl.services.UserServiceImpl;
import com.plusplus.serviceimpl.utils.MapperService;
import com.plusplus.vo.models.Transaction;
import com.plusplus.vo.models.TransactionResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Vostro on 23/02/2019.
 */
@RestController
@RequestMapping("/transaction")
@Api(value="jenius-transaction", description="Alpha Project Jenius System")
public class TransactionController {
    @Autowired
    private TransactionServiceImpl transactionService;
    @Autowired
    private DozerBeanMapper mapper;
    @Autowired
    private MapperService mapperService;

    @ApiOperation(value = "Insert Transaction")
    @RequestMapping(value = "insert_transaction", method= RequestMethod.POST, produces = "application/json")
    public boolean saveTransaction(@RequestBody Transaction transaction){
        try {
            transactionService.saveTransaction(transaction);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    @ApiOperation(value = "Insert Transaction List")
    @RequestMapping(value = "insert_transaction_list", method= RequestMethod.POST, produces = "application/json")
    public boolean saveTransactionList(@RequestBody List<Transaction> transactionList){
        try {
            transactionService.saveTransactionList(transactionList);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    @ApiOperation(value = "Get Transaction Monthly")
    @RequestMapping(value = "/get_transaction_monthly/{cashTag}", method= RequestMethod.GET, produces = "application/json")
    public List<TransactionResponse> getTransactionMonthly(@PathVariable String cashTag, Model model){
        return transactionService.getTransactionMonthly(cashTag);
    }

    @ApiOperation(value = "Get Transaction Daily")
    @RequestMapping(value = "/get_transaction_daily/{cashTag}", method= RequestMethod.GET, produces = "application/json")
    public List<TransactionResponse> getTransactionDaily(@PathVariable String cashTag, Model model){
        return transactionService.getTransactionDaily(cashTag);
    }

    @ApiOperation(value = "Generate Transaction")
    @RequestMapping(value = "generate_transaction", method= RequestMethod.GET, produces = "application/json")
    public boolean generateTransaction(Model model){
        transactionService.generateTransactionSampling();
        return true;
    }
}
