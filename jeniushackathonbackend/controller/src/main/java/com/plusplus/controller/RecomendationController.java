package com.plusplus.controller;

import com.plusplus.serviceimpl.services.RecomendationServiceImpl;
import com.plusplus.serviceimpl.services.TransactionServiceImpl;
import com.plusplus.serviceimpl.utils.MapperService;
import com.plusplus.vo.models.SavingRecomendation;
import com.plusplus.vo.models.TransactionResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by Vostro on 23/02/2019.
 */
@RestController
@RequestMapping("/recomendation")
@Api(value="jenius-recomendation", description="Alpha Project Jenius System")
public class RecomendationController {
    @Autowired
    private RecomendationServiceImpl recomendationService;
    @Autowired
    private DozerBeanMapper mapper;
    @Autowired
    private MapperService mapperService;

    @ApiOperation(value = "Get Saving Recomendation")
    @RequestMapping(value = "/get_saving_recomendation/{cashTag}", method= RequestMethod.GET, produces = "application/json")
    public SavingRecomendation getSavingRecomendation(@PathVariable String cashTag, Model model){
        return recomendationService.getSavingRecomendation(cashTag);
    }

    @ApiOperation(value = "Get Investment Recomendation")
    @RequestMapping(value = "/get_investment_recomendation/{cashTag}", method= RequestMethod.GET, produces = "application/json")
    public SavingRecomendation getInvestmentRecomendation(@PathVariable String cashTag, Model model){
        return recomendationService.getSavingRecomendation(cashTag);
    }
}
