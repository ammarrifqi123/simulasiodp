package com.plusplus.controller;

import com.plusplus.service.SurveyService;
import com.plusplus.serviceimpl.services.DataServiceImpl;
import com.plusplus.serviceimpl.services.SurveyServiceImpl;
import com.plusplus.serviceimpl.utils.MapperService;
import com.plusplus.vo.models.Survey;
import com.plusplus.vo.models.UMR;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Vostro on 24/02/2019.
 */
@RestController
@RequestMapping("/data")
@Api(value="jenius-data", description="Alpha Project Jenius System")
public class DataController {
    @Autowired
    private DataServiceImpl dataService;
    @Autowired
    private SurveyServiceImpl surveyService;
    @Autowired
    private DozerBeanMapper mapper;
    @Autowired
    private MapperService mapperService;

    @ApiOperation(value = "Insert UMR List")
    @RequestMapping(value = "insert_umr_list", method= RequestMethod.POST, produces = "application/json")
    public boolean saveUserList(@RequestBody List<UMR> umrList){
        try {
            dataService.addAllLocation(umrList);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    @ApiOperation(value = "Get all UMR")
    @RequestMapping(value = "/get_umr", method= RequestMethod.GET, produces = "application/json")
    public List<UMR> getAllUMR(Model model){
        return dataService.getAllLocation();
    }

    @ApiOperation(value = "Insert Survey")
    @RequestMapping(value = "insert_survey", method= RequestMethod.POST, produces = "application/json")
    public boolean saveSurvey(@RequestBody Survey survey){
        try {
            surveyService.addSurvey(survey);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    @ApiOperation(value = "Get all Survey")
    @RequestMapping(value = "/get_all_survey", method= RequestMethod.GET, produces = "application/json")
    public List<Survey> getAllSurvey(Model model){
        return surveyService.getAllSurvey();
    }
}
