package com.plusplus.controller;

import com.plusplus.serviceimpl.services.InvestmentServiceImpl;
import com.plusplus.serviceimpl.utils.MapperService;
import com.plusplus.vo.models.Obligation;
import com.plusplus.vo.models.Reksadana;
import com.plusplus.vo.models.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Vostro on 24/02/2019.
 */
@RestController
@RequestMapping("/investment")
@Api(value="jenius-investment", description="Alpha Project Jenius System")
public class InvestmentController {
    @Autowired
    private InvestmentServiceImpl investmentService;
    @Autowired
    private DozerBeanMapper mapper;
    @Autowired
    private MapperService mapperService;

    @ApiOperation(value = "Insert Obligation List")
    @RequestMapping(value = "insert_obligation_list", method= RequestMethod.POST, produces = "application/json")
    public boolean saveObligationList(@RequestBody List<Obligation> obligationList){
        try {
            investmentService.addObligationList(obligationList);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    @ApiOperation(value = "Get Obligation by couponSerial")
    @RequestMapping(value = "/get_obligation/{couponSerial}", method= RequestMethod.GET, produces = "application/json")
    public Obligation getObligation(@PathVariable String couponSerial, Model model){
        return investmentService.getObligationByCouponSerial(couponSerial);
    }

    @ApiOperation(value = "Get all Obligation")
    @RequestMapping(value = "/get_all_obligation", method= RequestMethod.GET, produces = "application/json")
    public List<Obligation> getAllObligation(Model model){
        return investmentService.getAllObligation();
    }

    @ApiOperation(value = "Insert Reksadana List")
    @RequestMapping(value = "insert_reksadana_list", method= RequestMethod.POST, produces = "application/json")
    public boolean saveReksadanaList(@RequestBody List<Reksadana> reksadanaList){
        try {
            investmentService.addReksadanaList(reksadanaList);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    @ApiOperation(value = "Get all Reksadana")
    @RequestMapping(value = "/get_all_reksadana", method= RequestMethod.GET, produces = "application/json")
    public List<Reksadana> getAllReksadana(Model model){
        return investmentService.getAllReksadana();
    }
}
