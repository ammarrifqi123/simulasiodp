package com.plusplus.controller;

import com.plusplus.serviceimpl.services.UserServiceImpl;
import com.plusplus.serviceimpl.utils.MapperService;
import com.plusplus.vo.models.LoginRequest;
import com.plusplus.vo.models.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Vostro on 23/02/2019.
 */
@RestController
@RequestMapping("/user")
@Api(value="jenius-user", description="Alpha Project Jenius System")
public class UserController {
    @Autowired
    private UserServiceImpl userService;
    @Autowired
    private DozerBeanMapper mapper;
    @Autowired
    private MapperService mapperService;

    @ApiOperation(value = "Insert User")
    @RequestMapping(value = "insert_user", method= RequestMethod.POST, produces = "application/json")
    public boolean saveUser(@RequestBody User user){
        try {
            userService.saveUser(user);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    @ApiOperation(value = "Insert User List")
    @RequestMapping(value = "insert_user_list", method= RequestMethod.POST, produces = "application/json")
    public boolean saveUserList(@RequestBody List<User> userList){
        try {
            userService.saveUserList(userList);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    @ApiOperation(value = "Get All User")
    @RequestMapping(value = "get_all_user", method= RequestMethod.GET, produces = "application/json")
    public List<User> getAllUser(Model model){
        return userService.getUserList();
    }

    @ApiOperation(value = "Get User by cashTag")
    @RequestMapping(value = "/get_user/{cashTag}", method= RequestMethod.GET, produces = "application/json")
    public User getUser(@PathVariable String cashTag, Model model){
        return userService.getUser(cashTag);
    }

    @ApiOperation(value = "Login")
    @RequestMapping(value = "login", method= RequestMethod.POST, produces = "application/json")
    public boolean login(@RequestBody LoginRequest request){
        try {
            return userService.login(request.getCashTag(), request.getPassword());
        }catch (Exception e){
            return false;
        }
    }
}
