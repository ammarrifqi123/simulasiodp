package com.plusplus.vo.models;

import lombok.*;

/**
 * Created by Vostro on 24/02/2019.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class UMR {
    private int locationId;
    private String location;
    private Double UMR;
}
