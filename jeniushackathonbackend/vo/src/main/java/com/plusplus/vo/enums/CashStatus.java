package com.plusplus.vo.enums;

/**
 * Created by Vostro on 23/02/2019.
 */
public enum CashStatus {
    SAFE,
    NOT_SAFE;
}
