package com.plusplus.vo.models;

import lombok.*;

/**
 * Created by Vostro on 23/02/2019.
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class TransactionResponse {
    private Double cashOut;
    private Double balanceAmount;
    private String timeVariable;
}
