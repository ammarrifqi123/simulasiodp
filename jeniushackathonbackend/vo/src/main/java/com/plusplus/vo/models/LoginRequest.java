package com.plusplus.vo.models;

import lombok.*;

/**
 * Created by Vostro on 23/02/2019.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class LoginRequest {
    private String cashTag;
    private String password;
}
