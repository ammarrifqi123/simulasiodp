package com.plusplus.vo.models;

import com.plusplus.vo.enums.CashStatus;
import lombok.*;

/**
 * Created by Vostro on 23/02/2019.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class User {
    private String cashTag;
    private String name;
    private String email;
    private String phoneNumber;
    private Double savingAmount;
    private String profilImageURL;
    private CashStatus cashStatus;
    private String password;
}
