package com.plusplus.vo.models;

import lombok.*;

/**
 * Created by Vostro on 24/02/2019.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class Survey {
    private String jenisPekerjaan;
    private String kotaTinggal;
    private String jumlahTanggungan;
    private String toleransiRisiko;
}
