package com.plusplus.vo.models;

import lombok.*;

/**
 * Created by Vostro on 23/02/2019.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class SavingRecomendation {
    private Double outcomeThisMonth;
    private Double savingThisMonth;
    private Double savingRecomendation;
    private Double investmentRecomendation;
}
